<?php
interface ICodeWriting {}
interface ICodeTesting {}
interface ICommunicationWithManager {}
interface IDesigning {}
interface ISetTasks {}

abstract class Employee {        
    public function getTypesOfWork() {
        $types = '';

        if ($this instanceof ICodeWriting)
            $types .= "code writing \n";
        if ($this instanceof ICodeTesting)
            $types .= "code testing \n";
        if ($this instanceof ICommunicationWithManager)
            $types .= "communication with manager \n";
        if ($this instanceof IDesigning)
            $types .= "designing \n";
        if ($this instanceof ISetTasks)
            $types .= "set tasks \n";

        return $types;
    }
}

class Developer extends Employee implements ICodeWriting, ICodeTesting, ICommunicationWithManager { }
class Designer extends Employee implements IDesigning, ICommunicationWithManager {}
class Tester extends Employee implements ICodeTesting, ICommunicationWithManager, ISetTasks {}
class Manager extends Employee implements ISetTasks {}

echo "developer: <br/>";
$developer = new Developer();
echo $developer->getTypesOfWork();

echo "<br/>";

echo "designer: <br/>";
$designer = new Designer();
echo $designer->getTypesOfWork();

echo "<br/>";

echo "tester: <br/>";
$tester = new Tester();
echo $tester->getTypesOfWork();

echo "<br/>";

echo "manager: <br/>";
$manager = new Manager();
echo $manager->getTypesOfWork();